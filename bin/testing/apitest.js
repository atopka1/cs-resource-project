// var curl = require("curlrequest")

// var options = {
//     url: 'https://api.github.com/users/octocat'
//     , useragent: "User Agent: atopka1@gmail.com"
// };

// curl.request(options, function (err, data) {
//     console.log(data);
// }).catch(function(res) {

//     if (res instanceof Error) {
//         console.log('error', res);
//     } else {
//         console.log('it worked!');
//     }
// })

// ;



/* This works */
var request = require('request');
var jsonfile = require('jsonfile');



function test() {

    getListOfRepoDescriptions(99);

}

test();

function tryGitHub() {

    var file = './github'+Date.now().toString()+'.json';

    const options = {
        url: 'https://api.github.com/repositories?since=500',
        headers: { 'User-Agent': 'atopka1@gmail.com' }
    };

    request(options, function (error, response, body) {

        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            jsonfile.writeFile(file, info, function(err) {
                console.log("Wrote to " + file);
            })
        
        }
        else {
            console.log(error);
            console.log(response);
        }
    })

}


function getListOfRepoDescriptions(num_repos) {

    /*
        Loop through x number of repos and add description to object
        so like 
        {
            repo_id: number,
            description: string
        }
        and then write all that to a file
    */

    var baseUrl = "https://api.github.com/repositories";
    var options = {
        url: baseUrl,
        headers: { 'User-Agent': 'atopka1@gmail.com' }
    }

    var result = {
        "descriptions" : []
    };

    var file = "repo-descriptions-" + Date.now().toString() + ".txt";

    jsonfile.writeFile(file, result, function (err) {
        console.log("Created " + file);
    });


    var i = 0;
    while (i<num_repos) {

        options.url = baseUrl + "?since="+ (i+99);

        request(options, function (error, response, body) {

            if (!error && response.statusCode == 200) {
                var info = JSON.parse(body);

                for (j = 0; j<info.length; j++) {
                    result["descriptions"].push({
                        "repo_id": info[j]["id"],
                        "description": info[j]["description"]
                    });
                    
                }

                jsonfile.writeFile(file, result, { flag: 'a' }, function (err) {});

            }
            else {
                console.log(error);
            }
        });


        i = i + 99;
    }

    // var file = "repo-descriptions-"+Date.now().toString()+".txt";
    // jsonfile.writeFile(file, result, function (err) {
    //     console.log("Wrote to " + file);
    // });

}




